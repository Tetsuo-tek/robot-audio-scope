#include "robotaudioscope.h"
#include <Core/System/skosenv.h>
#include <Core/Containers/skarraycast.h>
#include "gui.h"

ConstructorImpl(RobotAudioScope, SkFlowSat)
{
    window = nullptr;
    scope = nullptr;
    vuMeter = nullptr;
    chansCombo = nullptr;

    subscriber = nullptr;

    selectedTimeMult = 0;

    timeMults[0] = 1;
    timeMults[1] = 2;
    timeMults[2] = 4;
    timeMults[3] = 8;
    timeMults[4] = 10;
    timeMults[5] = 20;
    timeMults[6] = 40;
    timeMults[7] = 80;
    timeMults[8] = 100;
    timeMults[9] = 120;
    timeMults[10] = 240;
    timeMults[11] = 480;
    timeMults[12] = 1000;

    vuChanBarWidth = 6;

    chanColors[0] = SkColor::red().toFlColor();
    chanColors[1] = SkColor::green().toFlColor();
    chanColors[2] = SkColor::purple().toFlColor();
    chanColors[3] = SkColor::magenta().toFlColor();
    chanColors[4] = SkColor::yellow().toFlColor();
    chanColors[5] = SkColor::maroon().toFlColor();
    chanColors[6] = SkColor::darkCyan().toFlColor();
    chanColors[7] = SkColor::blue().toFlColor();

    setObjectName("RobotAudioScope");

    SlotSet(onSubscriberReady);

    SlotSet(fullScreen);
    SlotSet(selectChannel);
    SlotSet(curveTypeSelection);
    SlotSet(tickTimeSelection);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotAudioScope::buildUI()
{
    SkVariant indexVal;
    indexVal.fromJson(GUI_INDEX);
    indexVal.copyToMap(guiIndex);

    int index = guiIndex["gui/mainwindow.json"].toInt();
    window = DynCast(SkFltkDoubleWindow, ui->loadFromJSON(gui[index].data));
    Attach(window, closed, this, quit, SkQueued);

    scope = DynCast(SkFltkCurvesChart, ui->get("scope"));
    vuMeter = DynCast(SkFltkBarChart, ui->get("vuMeter"));

    SkFltkLightButton *fsButton = DynCast(SkFltkLightButton, ui->get("fsButton"));
    Attach(fsButton, clicked, this, fullScreen, SkQueued);

    chansCombo = DynCast(SkFltkComboBox, ui->get("chansCombo"));
    Attach(chansCombo, picked, this, selectChannel, SkQueued);

    SkFltkComboBox *curveTypeCombo = DynCast(SkFltkComboBox, ui->get("curveTypeCombo"));
    Attach(curveTypeCombo, picked, this, curveTypeSelection, SkAttachMode::SkQueued);

    SkFltkComboBox *periodCombo = DynCast(SkFltkComboBox, ui->get("periodCombo"));
    Attach(periodCombo, picked, this, tickTimeSelection, SkAttachMode::SkQueued);

    SkFltkButton *quitButton = DynCast(SkFltkButton, ui->get("quitButton"));
    Attach(quitButton, clicked, this, quit, SkQueued);

    window->show();

    curveTypeCombo->setCurrentIndex(3);
    periodCombo->setCurrentIndex(0);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotAudioScope::onSetup()
{
    subscriber = new SkFlowAudioSubscriber(this);
    Attach(subscriber, ready, this, onSubscriberReady, SkQueued);

    setupSubscriber(subscriber);

    return true;
}

void RobotAudioScope::onInit()
{
    buildUI();
}

void RobotAudioScope::onQuit()
{
    window = nullptr;
    scope = nullptr;
    vuMeter = nullptr;
    chansCombo = nullptr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotAudioScope::onChannelAdded(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);
    AssertKiller(!ch);

    if (!chansCombo)
        return;

    if (ch->flow_t == FT_AUDIO_DATA || ch->flow_t == FT_AUDIO_PREVIEW_DATA)
        chansCombo->add(ch->name.c_str());
}

void RobotAudioScope::onChannelRemoved(SkFlowChanID chanID)
{
    if (!chansCombo)
        return;

    SkFlowChannel *ch = channel(chanID);
    AssertKiller(!ch);

    int index = chansCombo->find(ch->name.c_str());

    if (index > -1)
        chansCombo->remove(index);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotAudioScope::onFlowDataCome(SkFlowChannelData &chData)
{
    if (!subscriber->isReady() || !scope || !vuMeter)
        return;

    if (chData.chanID == subscriber->source()->chanID)
    {
        uint channels = subscriber->parameters().getChannels();
        uint bufferFrames = subscriber->parameters().getBufferFrames();

        for(uint z=0; z<channels; z++)
            for(uint i=0; i<bufferFrames; i+=timeMults[selectedTimeMult])
            {
                if (scope->curve(z)->samples.count() > scope->w())
                    scope->curve(z)->samples.dequeue();

                scope->curve(z)->samples.enqueue(subscriber->internalPlanarBuffer()[z][i]);
            }
    }

    else if (subscriber->hasVuChannel() && chData.chanID == subscriber->vuChannel()->chanID)
    {
        float *vu = SkArrayCast::toFloat(chData.data.toVoid());
        uint channels = subscriber->parameters().getChannels();

        for(uint z=0; z<channels; z++)
            vuMeter->getMeter(z)->value = vu[z];

        vuMeter->redraw();
    }

    else if (subscriber->hasClippingChannel() && chData.chanID == subscriber->clippingChannel()->chanID)
    {
        uint channels = subscriber->parameters().getChannels();
        uint chanIndex = SkArrayCast::toChar(chData.data.toVoid())[0];

        chronoVu.start();

        if (chanIndex >= channels)
        {
            ObjectError("Channel value NOT valid: " << chanIndex);
            return;
        }

        vuMeter->getMeter(chanIndex)->alert = true;
        vuMeter->redraw();
    }
}

void RobotAudioScope::onFastTick()
{
    if (!subscriber->isReady() || !scope || !vuMeter)
        return;

    if (chronoVu.stop() >= 0.3)
    {
        uint channels = subscriber->parameters().getChannels();

        for(uint z=0; z<channels; z++)
            vuMeter->getMeter(z)->alert = false;

        vuMeter->redraw();
    }


    scope->redraw();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotAudioScope, onSubscriberReady)
{
    SilentSlotArgsWarning();

    scope->clear();
    vuMeter->clear();

    SkFltkCurve *c = new SkFltkCurve;
    c->enabled = true;
    c->color = SkColor::yellow().toFlColor();

    uint channels = subscriber->parameters().getChannels();

    int vuWidth = (vuChanBarWidth+2)*channels;
    vuMeter->resize(SkRegion(window->w()-vuWidth, 0, vuWidth, 0));

    for(uint z=0; z<channels; z++)
    {
        SkFltkCurve *c = new SkFltkCurve;
        c->enabled = true;

        if (z <= 7)
            c->color = chanColors[z];

        else
            c->color = SkColor::white().toFlColor();

        scope->addCurve(c);

        SkFltkMeterBar *b = new SkFltkMeterBar;
        b->enabled = true;
        b->value = 0.f;
        b->lastPeakHolderSegNum = 0;
        b->alert = false;
        vuMeter->addMeter(b);
    }

    //  //  //  //  //

    scope->redraw();
    vuMeter->redraw();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotAudioScope, fullScreen)
{
    SilentSlotArgsWarning();

    static bool fullscreen = false;

    if (!fullscreen)
        window->enableFullScreen();

    else
        window->disableFullScreen();

    window->redraw();
    fullscreen = !fullscreen;
}

SlotImpl(RobotAudioScope, selectChannel)
{
    SilentSlotArgsWarning();

    SkFlowChannel *ch = channel(chansCombo->currentText());
    AssertKiller(!ch);

    if (subscriber->isReady())
    {
        subscriber->unsubscribe();
        //return; TEST
    }

    subscriber->enableVuChan(true);
    subscriber->enableClippingChan(true);

    subscriber->subscribe(ch->name.c_str());
}

SlotImpl(RobotAudioScope, curveTypeSelection)
{
    SilentSlotArgsWarning();

    SkFltkCurveType curveType = Arg_Enum(SkFltkCurveType);
    scope->setCurveType(curveType);
}

SlotImpl(RobotAudioScope, tickTimeSelection)
{
    SilentSlotArgsWarning();

    for(ULong z=0; z<scope->count(); z++)
        scope->curve(z)->samples.clear();

    selectedTimeMult = Arg_Int;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
