#ifndef ROBOTAUDIOSCOPE_H
#define ROBOTAUDIOSCOPE_H

#include <Core/System/Network/FlowNetwork/skflowsat.h>
#include <Core/System/Network/FlowNetwork/skflowaudiosubscriber.h>

#include <UI/FLTK/skfltkui.h>
#include <UI/FLTK/skfltkwindows.h>
#include <UI/FLTK/skfltkhlayout.h>
#include <UI/FLTK/skfltkvlayout.h>
#include <UI/FLTK/skfltkbuttons.h>
#include <UI/FLTK/skfltkmenus.h>
#include <UI/FLTK/skfltkviews.h>
#include <UI/FLTK/skfltkcurvechart.h>
#include <UI/FLTK/skfltkbarchart.h>

class RobotAudioScope extends SkFlowSat
{
    SkFlowAudioSubscriber *subscriber;

    SkTreeMap<SkString, SkVariant> guiIndex;

    SkFltkDoubleWindow *window;
    SkFltkCurvesChart *scope;
    SkFltkBarChart *vuMeter;
    SkFltkComboBox *chansCombo;

    uint vuChanBarWidth;

    Fl_Color chanColors[8];
    SkElapsedTime chronoVu;

    int selectedTimeMult;
    ulong timeMults[13];

    public:
        Constructor(RobotAudioScope, SkFlowSat);

        Slot(onSubscriberReady);

        Slot(fullScreen);
        Slot(selectChannel);
        Slot(curveTypeSelection);
        Slot(tickTimeSelection);

    private:
        void buildUI();

        bool onSetup()                                  override;
        void onInit()                                   override;
        void onQuit()                                   override;

        void onChannelAdded(SkFlowChanID chanID)        override;
        void onChannelRemoved(SkFlowChanID chanID)      override;

        void onFlowDataCome(SkFlowChannelData &chData)  override;

        void onFastTick()                               override;
};

#endif // ROBOTAUDIOSCOPE_H
