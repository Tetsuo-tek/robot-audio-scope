#ifndef GUI_H
#define GUI_H

const char *gui_rootdir = "gui";
const unsigned gui_count = 1;

const struct {
	const bool isDir;
	const char *relativePath;
	const char *name;
	const unsigned size;
	const char *data;
} gui[1] = {
	{
		false,
		"gui/",
		"mainwindow.json",
		5549,
		"{\n" \
		"    \"name\" : \"window\",\n" \
		"    \"type\" : \"DoubleWindow\",\n" \
		"    \"title\" : \"Audio scope\",\n" \
		"    \"region\" : [0, 0, 800, 480],\n" \
		"    \"color\" : [0, 0, 0],\n" \
		"    \"layout\" : {\n" \
		"        \"type\" : \"HLayout\",\n" \
		"        \"spacing\" : 4,\n" \
		"        \"stretch\" : true,\n" \
		"        \"children\" : [\n" \
		"            {\n" \
		"                \"name\" : \"scope\",\n" \
		"                \"type\" : \"CurvesChart\",\n" \
		"                \"color\" : [0, 0, 0],\n" \
		"                \"bounds\" : [-1.0, 1.0],\n" \
		"                \"curve\" : \"LineSpike\"\n" \
		"            },\n" \
		"            {\n" \
		"                \"name\" : \"vuMeter\",\n" \
		"                \"type\" : \"BarChart\",\n" \
		"                \"size\" : [10, 0],\n" \
		"                \"resizePolicy\" : \"FIXED_WIDTH\",\n" \
		"                \"color\" : [20, 20, 20],\n" \
		"                \"bounds\" : [-25.0, 0.0],\n" \
		"                \"gradient\" : true,\n" \
		"                \"enablePeakHolderLeds\" : true,\n" \
		"                \"peakColor\" : [0, 255, 0],\n" \
		"                \"segmentColor_ON\" : [255, 255, 0],\n" \
		"                \"segmentColor_OFF\" : [50, 50, 50]\n" \
		"            }\n" \
		"        ],\n" \
		"        \"footer\" : {\n" \
		"            \"height\" : 25,\n" \
		"            \"type\" : \"HLayout\",\n" \
		"            \"spacing\" : 4,\n" \
		"            \"stretch\" : true,\n" \
		"            \"box\" : \"FLAT_BOX\",\n" \
		"            \"color\" : [30, 30, 30],\n" \
		"            \"children\" : [\n" \
		"                {\n" \
		"                    \"name\" : \"fsButton\",\n" \
		"                    \"type\" : \"LightButton\",\n" \
		"                    \"color\" : [60, 60, 60],\n" \
		"                    \"size\" : [100, 0],\n" \
		"                    \"resizePolicy\" : \"FIXED_WIDTH\",\n" \
		"                    \"labelFontColor\" : [255, 255, 255],\n" \
		"                    \"labelFont\" : \"SCREEN_BOLD\",\n" \
		"                    \"labelFontSize\" : 12,\n" \
		"                    \"label\" : \"Fullscreen\",\n" \
		"                    \"tooltip\" : \"Toggle fullscreen\",\n" \
		"                    \"align\" : \"ALIGN_CENTER\"\n" \
		"                },\n" \
		"                {\n" \
		"                    \"name\" : \"chansLabel\",\n" \
		"                    \"type\" : \"Box\",\n" \
		"                    \"color\" : [60, 60, 60],\n" \
		"                    \"size\" : [80, 0],\n" \
		"                    \"resizePolicy\" : \"FIXED_WIDTH\",\n" \
		"                    \"labelFontColor\" : [255, 255, 255],\n" \
		"                    \"labelFont\" : \"SCREEN_BOLD\",\n" \
		"                    \"labelFontSize\" : 12,\n" \
		"                    \"label\" : \"Channels:\",\n" \
		"                    \"align\" : [\"ALIGN_RIGHT\", \"ALIGN_INSIDE\"]\n" \
		"                },\n" \
		"                {\n" \
		"                    \"name\" : \"chansCombo\",\n" \
		"                    \"type\" : \"ComboBox\",\n" \
		"                    \"labelFontColor\" : [255, 255, 255],\n" \
		"                    \"labelFont\" : \"SCREEN_BOLD\",\n" \
		"                    \"labelFontSize\" : 12,\n" \
		"                    \"tooltip\" : \"Select the input channel\"\n" \
		"                },\n" \
		"                {\n" \
		"                    \"name\" : \"curveTypeLabel\",\n" \
		"                    \"type\" : \"Box\",\n" \
		"                    \"size\" : [60, 0],\n" \
		"                    \"resizePolicy\" : \"FIXED_WIDTH\",\n" \
		"                    \"labelFontColor\" : [255, 255, 255],\n" \
		"                    \"labelFont\" : \"SCREEN_BOLD\",\n" \
		"                    \"labelFontSize\" : 12,\n" \
		"                    \"label\" : \"Curve:\",\n" \
		"                    \"align\" : [\"ALIGN_RIGHT\", \"ALIGN_INSIDE\"]\n" \
		"                },\n" \
		"                {\n" \
		"                    \"name\" : \"curveTypeCombo\",\n" \
		"                    \"type\" : \"ComboBox\",\n" \
		"                    \"labelFontColor\" : [255, 255, 255],\n" \
		"                    \"labelFont\" : \"SCREEN_BOLD\",\n" \
		"                    \"labelFontSize\" : 12,\n" \
		"                    \"tooltip\" : \"Select curve type\",\n" \
		"                    \"items\" : [\n" \
		"                        \"Point\",\n" \
		"                        \"PointSpike\",\n" \
		"                        \"Line\",\n" \
		"                        \"LineSpike\",\n" \
		"                        \"Step\",\n" \
		"                        \"StepSpike\",\n" \
		"                        \"Spike\"\n" \
		"                    ]\n" \
		"                },\n" \
		"                {\n" \
		"                    \"name\" : \"periodLabel\",\n" \
		"                    \"type\" : \"Box\",\n" \
		"                    \"label\" : \"Period:\",\n" \
		"                    \"size\" : [60, 0],\n" \
		"                    \"resizePolicy\" : \"FIXED_WIDTH\",\n" \
		"                    \"labelFontColor\" : [255, 255, 255],\n" \
		"                    \"labelFont\" : \"SCREEN_BOLD\",\n" \
		"                    \"labelFontSize\" : 12,\n" \
		"                    \"align\" : [\"ALIGN_RIGHT\", \"ALIGN_INSIDE\"]\n" \
		"                },\n" \
		"                {\n" \
		"                    \"name\" : \"periodCombo\",\n" \
		"                    \"type\" : \"ComboBox\",\n" \
		"                    \"tooltip\" : \"Select period multiplier\",\n" \
		"                    \"labelFontColor\" : [255, 255, 255],\n" \
		"                    \"labelFont\" : \"SCREEN_BOLD\",\n" \
		"                    \"labelFontSize\" : 12,\n" \
		"                    \"items\" : [\n" \
		"                        \"x 1 (real period)\",\n" \
		"                        \"x 2\",\n" \
		"                        \"x 4\",\n" \
		"                        \"x 8\",\n" \
		"                        \"x 10\",\n" \
		"                        \"x 20\",\n" \
		"                        \"x 40\",\n" \
		"                        \"x 80\",\n" \
		"                        \"x 160\",\n" \
		"                        \"x 320\",\n" \
		"                        \"x 640\",\n" \
		"                        \"x 1280\"\n" \
		"                    ]\n" \
		"                },\n" \
		"                {\n" \
		"                    \"type\" : \"LayoutSpacer\"\n" \
		"                },\n" \
		"                {\n" \
		"                    \"name\" : \"quitButton\",\n" \
		"                    \"type\" : \"Button\",\n" \
		"                    \"size\" : [70, 0],\n" \
		"                    \"resizePolicy\" : \"FIXED_WIDTH\",\n" \
		"                    \"color\" : [60, 60, 60],\n" \
		"                    \"labelFontColor\" : [255, 255, 255],\n" \
		"                    \"labelFont\" : \"SCREEN_BOLD\",\n" \
		"                    \"labelFontSize\" : 12,\n" \
		"                    \"label\" : \"Quit\",\n" \
		"                    \"tooltip\" : \"Close application\",\n" \
		"                    \"align\" : \"ALIGN_CENTER\"\n" \
		"                }\n" \
		"            ]\n" \
		"        }\n" \
		"    }\n" \
		"}\n"
	}
};

#define GUI_INDEX	"{\"gui/mainwindow.json\":0}"

#endif // GUI_H
